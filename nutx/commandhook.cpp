#include "nutx.h"
#include "gamehook.h"

using _send_gcommand = INT(*)(PCWCHAR str);
const _send_gcommand send_gcommand = (_send_gcommand)0x951620;

INT sendCommand(PCWCHAR str, INT type)
{
	if (wcsncmp(L"//", str, 2) != 0)
		return send_gcommand(str);

	if (!wcsncmp(L"//dofile ", str, 9))
	{
		str += 9;
		HSQUIRRELVM vm = sq_getvm();
		sq_pushroottable(vm);
		sqstd_dofile(vm, str, 0, 1);
		sq_poptop(vm);
		gamex::send_notice(L"脚本加载执行完成", rgb(0, 255, 0), 17);
		return 1;
	}

	return send_gcommand(str);
}

void initCommandHook()
{
	//95C103 - E8 0859FFFF - call DNF.exe+551A10
	BYTE hookedBytes[5] = { 0xE8,0x90,0x90,0x90,0x90 };
	*(DWORD*)(hookedBytes + 1) = (DWORD)sendCommand - (DWORD)0x95C103 - 5;
	WriteProcessMemory(INVALID_HANDLE_VALUE, (LPVOID)0x95C103, hookedBytes, 5, NULL);
}