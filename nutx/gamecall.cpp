#include "nutx.h"
#include "gamecall.h"

//公告call
using _game_notice = void(__fastcall*)(UINT ecx, UINT edx, PCWCHAR str, INT rgb, INT type, INT n1, INT n2, INT n3);
const _game_notice game_notice = (_game_notice)0x9536C0;
//文本call
using _game_text1 = void(__fastcall*)(UINT ecx, UINT edx, INT type);
const _game_text1 game_text1 = (_game_text1)0x1206550;
using _game_text2 = void(__fastcall*)(UINT ecx, UINT edx, INT x, INT y, INT rgb, PCWCHAR str);
const _game_text2 game_text2 = (_game_text2)0x1206BD0;
using _game_text3 = void(__fastcall*)(UINT ecx, UINT edx);
const _game_text3 game_text3 = (_game_text3)0x1206570;
//发包call
constexpr UINT SOCKET_ADDR = 0x1AEB6E4;
using _game_start_header = void(__fastcall*)(UINT ecx, UINT edx, INT header);
const _game_start_header game_start_header = (_game_start_header)0x1127D60;
using _game_write_data1 = void(__fastcall*)(UINT ecx, UINT edx, BYTE data);
const _game_write_data1 game_write_data1 = (_game_write_data1)0x1128550;
using _game_write_data2 = void(__fastcall*)(UINT ecx, UINT edx, WORD data);
const _game_write_data2 game_write_data2 = (_game_write_data2)0x1128580;
using _game_write_data4 = void(__fastcall*)(UINT ecx, UINT edx, INT32 data);
const _game_write_data4 game_write_data4 = (_game_write_data4)0x11285B0;
using _game_write_data8 = void(__fastcall*)(UINT ecx, UINT edx, INT64 data);
const _game_write_data8 game_write_data8 = (_game_write_data8)0x11285E0;
using _game_start_send = void(__fastcall*)(UINT ecx, UINT edx);
const _game_start_send game_start_send = (_game_start_send)0x1127EC0;

void gamex::send_notice(PCWCHAR str, INT rgb, INT type)
{
	UINT ecx = readVal(readVal(0x1A5FB20) + 0x40);
	game_notice(ecx, 0, str, rgb, type, 0, 0, 0);
}

void gamex::draw_text(INT x, INT y, INT rgb, PCWCHAR str)
{
	UINT ecx = readVal(0x1B45B94);
	game_text1(ecx, 0, 0x1A74360);
	game_text2(ecx, 0, x, y, rgb, str);
	game_text3(ecx, 0);
}

void gamex::start_dungeon(WORD code, BYTE type, BYTE ishell)
{
	UINT socket = readVal(SOCKET_ADDR);
	game_start_header(socket, 0, 15);
	game_start_send(socket, 0);

	game_start_header(socket, 0, 16);
	game_write_data2(socket, 0, code);
	game_write_data1(socket, 0, type);
	game_write_data1(socket, 0, ishell);
	game_write_data1(socket, 0, 0);
	game_start_send(socket, 0);
}

void gamex::exit_dungeon()
{
	UINT socket = readVal(SOCKET_ADDR);
	game_start_header(socket, 0, 45);
	game_start_send(socket, 0);
}
