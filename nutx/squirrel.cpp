#include "nutx.h"
#include "squirrel.h"

UINT registerFunction(PCWCHAR funName, SQFUNCTION funAddr)
{
    HSQUIRRELVM vm = sq_getvm();
    sq_pushroottable(vm);
    sq_pushstring(vm, funName, -1);
    sq_newclosure(vm, funAddr, 0);
    sq_newslot(vm, -3, 0);
    sq_poptop(vm);
    return 1;
}