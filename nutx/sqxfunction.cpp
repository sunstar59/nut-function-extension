#include "nutx.h"
#include "sqxfunction.h"

INT sqx_send_notice(HSQUIRRELVM vm)
{
	PCWCHAR str;
	sq_getstring(vm, 2, &str);
	INT rgb;
	sq_getinteger(vm, 3, &rgb);

	if (sq_gettop(vm) == 3) {
		gamex::send_notice(str, rgb);
		return 0;
	}

	INT type;
	sq_getinteger(vm, 4, &type);
	gamex::send_notice(str, rgb, type);
	return 0;
}

INT sqx_draw_text(HSQUIRRELVM vm)
{
	INT x, y;
	sq_getinteger(vm, 2, &x);
	sq_getinteger(vm, 3, &y);
	INT rgb;
	sq_getinteger(vm, 4, &rgb);
	PCWCHAR str;
	sq_getstring(vm, 5, &str);

	gamex::draw_text(x, y, rgb, str);
	return 0;
}

INT sqx_start_dungeon(HSQUIRRELVM vm)
{
	INT code, type;
	sq_getinteger(vm, 2, &code);
	sq_getinteger(vm, 3, &type);

	if (sq_gettop(vm) == 3) {
		gamex::start_dungeon(code, type);
		return 0;
	}

	INT ishell;
	sq_getinteger(vm, 4, &ishell);
	gamex::start_dungeon(code, type, ishell);
	return 0;
}

INT sqx_exit_dungeon(HSQUIRRELVM vm)
{
	gamex::exit_dungeon();
	return 0;
}