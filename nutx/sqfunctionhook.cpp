#include "nutx.h"
#include "gamehook.h"
#include "sqxfunction.h"

void startRegistNewSqxFunction()
{
	registerFunction(L"sqx_send_notice", sqx_send_notice);
	registerFunction(L"sqx_draw_text", sqx_draw_text);
	registerFunction(L"sqx_start_dungeon", sqx_start_dungeon);
	registerFunction(L"sqx_exit_dungeon", sqx_exit_dungeon);
}

void initSqFuncHook()
{
	BYTE hookedBytes[5] = { 0xE8,0x90,0x90,0x90,0x90 };
	*(DWORD*)(hookedBytes + 1) = (DWORD)startRegistNewSqxFunction - (DWORD)0x609D0E - 5;
	WriteProcessMemory(INVALID_HANDLE_VALUE, (LPVOID)0x609D0E, hookedBytes, 5, NULL);
}