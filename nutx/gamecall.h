#pragma once

namespace gamex
{
	#define _v(v1,v2) ((v1|v2)<<8)
	#define rgb(r,g,b) ((BYTE)r|_v((BYTE)g,_v((BYTE)b,0xFFFFFF00)))
	#define rgba(r,g,b,a) ((BYTE)r|_v((BYTE)g,_v((BYTE)b,(BYTE)a<<8)))

	//type 14 喇叭公告
	//type 17 系统公告
	//type 37 个人公告
	void send_notice(PCWCHAR str, INT rgb, INT type = 37);

	void draw_text(INT x, INT y, INT rgb, PCWCHAR str);

	void start_dungeon(WORD code, BYTE type, BYTE ishell = 0);

	void exit_dungeon();
}