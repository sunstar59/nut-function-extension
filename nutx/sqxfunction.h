#pragma once

//发送公告
//sqx_send_notice(文本,rgb,位置)
INT sqx_send_notice(HSQUIRRELVM vm);

//绘制文本
//sqx_draw_text(x,y,rgb,文本)
INT sqx_draw_text(HSQUIRRELVM vm);

//开始副本
//sqx_start_dungeon(编号,难度,是否深渊)
INT sqx_start_dungeon(HSQUIRRELVM vm);

//退出副本
//sqx_exit_dungeon()
INT sqx_exit_dungeon(HSQUIRRELVM vm);

