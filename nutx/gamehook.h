#pragma once

void initCommandHook();

void initSqFuncHook();

inline void initHook()
{
	//hook注册sq_DrawLine的函数
	//在里面注册我们新增的nut函数
	initSqFuncHook();

	//hook发送聊天信息的函数
	//dofile xxx加载客户端目录的nut脚本
	initCommandHook();
}