#pragma once

template<typename T = UINT>
inline T readVal(UINT address)
{
	return *(T*)(address);
}

template <typename T = UINT>
inline void writeVal(UINT address, T value)
{
	*(T*)(address) = value;
}

class SimpleHook
{
private:
	DWORD hookedAddr = NULL;
	BYTE hookedBytes[5] = { 0x00,0x00,0x00,0x00,0x00 };
	BYTE hookBytes[5] = { 0xE9,0x00,0x00,0x00,0x00 };

public:
	BOOL HookByAddress(DWORD hookedFunc, DWORD myHookFunc);
	BOOL HookByModule(LPCSTR lpModuleName, LPCSTR pszFuncName, DWORD myHookFunc);
	BOOL UnHook();
	BOOL ReHook();
};